package home.crsk.smssplasher;

/**
 * Created by criskey on 31/12/2016.
 */
class SMS {

    final String sender;
    final String phoneNumber;
    final String body;

    public SMS(String sender, String phoneNumber, String body) {
        this.sender = sender;
        this.phoneNumber = phoneNumber;
        this.body = body;
    }
}
