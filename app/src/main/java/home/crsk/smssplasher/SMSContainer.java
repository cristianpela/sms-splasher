package home.crsk.smssplasher;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by criskey on 31/12/2016.
 */
class SMSContainer {

    private List<SMS> mSMSCollection;

    private int mCurrentPosition = -1;

    public SMSContainer(List<SMS> mSMSCollection) {
        this.mSMSCollection = mSMSCollection;
    }

    public SMSContainer() {
        this.mSMSCollection = new LinkedList<>();
    }

    public void add(SMS sms) {
        this.mSMSCollection.add(sms);
    }

    public SMS next() {
        if (mCurrentPosition + 1 < mSMSCollection.size()) {
            mCurrentPosition++;
        }
        return mSMSCollection.get(mCurrentPosition);
    }

    public SMS previous() {
        if (mCurrentPosition - 1 < 0) {
            mCurrentPosition = 0;
        }else
            mCurrentPosition--;
        return mSMSCollection.get(mCurrentPosition);
    }

    public String getTrackInfo() {
        return (mCurrentPosition + 1) + "/" + mSMSCollection.size();
    }
}
