package home.crsk.smssplasher;

import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.support.v4.content.WakefulBroadcastReceiver;

import java.util.ArrayList;

public class SMSProxyReceiver extends WakefulBroadcastReceiver {

    public static final String ACTION_MOCK_SMS_RECEIVED = "home.crisk.smssplasher.MOCK_SMS_RECEIVED";
    public static final String EXTRA_MOCK_MESSAGES = "home.crisk.smssplasher.EXTRA_MOCK_MESSAGES";

    public static final String ACTION_SMS_RECEIVED = Telephony.Sms.Intents.SMS_RECEIVED_ACTION;


    public SMSProxyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(ACTION_MOCK_SMS_RECEIVED)) {
            ArrayList<String> rawSmsList = new ArrayList<>();
            rawSmsList.add("Ion;0726566545;Ce faci?");
            rawSmsList.add("Grigore;072333335;La multi ani?");
            rawSmsList.add("Vasile;074393945;Sugus?");
            startWakefulService(context, SMSDisplayService.mockSMSIntent(context, rawSmsList));
        } else if (action.equals(ACTION_SMS_RECEIVED)) {
            startWakefulService(context, SMSDisplayService.SMSIntent(context, intent));
        }
    }
}
