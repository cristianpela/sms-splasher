package home.crsk.smssplasher;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.WindowManager;

/**
 * Created by criskey on 31/12/2016.
 */

public final class Utils {

    private Utils() {
    }

    public static Pair<Integer, Integer> getScreenSize(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;
        int height = displaymetrics.heightPixels;
        return new Pair(width, height);
    }

    public static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return (contactName == null) ? "<Contact Not Saved>" : contactName;
    }


    public static Config getConfig(Context context){
        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(context);
        boolean hasTimeout = preference.getBoolean("pref_has_chk_timeout", false);
        int timeout = Integer.parseInt(preference.getString("pref_chk_timeout", "1"));
        return new Config(hasTimeout, timeout);
    }
}
