package home.crsk.smssplasher;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.IdRes;
import android.telephony.SmsMessage;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static android.view.View.GONE;

public class SMSDisplayService extends Service {

    private static final String TAG = SMSDisplayService.class.getSimpleName();

    private static long TIME;

    private View mSMSView;

    private CountDownTimer mClosingCountDown;

    private WindowManager mWindowManager;

    private boolean mShow;

    private SMSContainer mSMSContainer;

    private Config mConfig;

    public SMSDisplayService() {
        mSMSContainer = new SMSContainer();
    }

    /**
     * just for testing purposes
     * @param context
     * @param rawSmsList
     * @return
     */
    public static Intent mockSMSIntent(Context context, ArrayList<String> rawSmsList) {
        return new Intent(context, SMSDisplayService.class)
                .setAction(SMSProxyReceiver.ACTION_MOCK_SMS_RECEIVED).putExtra(SMSProxyReceiver.EXTRA_MOCK_MESSAGES, rawSmsList);
    }

    public static Intent SMSIntent(Context context, Intent phoneSMSIntent) {
        return new Intent(context, SMSDisplayService.class)
                .setAction(phoneSMSIntent.getAction())
                .putExtras(phoneSMSIntent);
    }

    private void initConfig() {
        if (mConfig == null) {
            mConfig = Utils.getConfig(this);
            TIME = TimeUnit.MINUTES.toMillis(mConfig.timeout);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        initConfig();
        String action = intent.getAction();
        if (action.equals(SMSProxyReceiver.ACTION_SMS_RECEIVED)) {
            decodeSMS(intent);
        } else
            mockDecodeSMS(intent);
        if (!mShow)
            showWindow();
        else {
            getTextView(R.id.txtTrackInfo).setText(mSMSContainer.getTrackInfo());
        }
        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        Log.e(TAG, "Service destroyed");
        if (mConfig.hasTimeout)
            mClosingCountDown.cancel();
        super.onDestroy();
    }

    private void startCountDown() {
        if (mClosingCountDown == null) {
            final ProgressBar progressBar = getProgressBar();
            final int max = progressBar.getMax();
            mClosingCountDown = new CountDownTimer(TIME, TimeUnit.SECONDS.toMillis(10)) {
                @Override
                public void onTick(long millisUntilFinished) {
                    float progress = ((TIME - millisUntilFinished) / (float) TIME) * max;
                    progressBar.setProgress((int) progress);
                }

                @Override
                public void onFinish() {
                    progressBar.setProgress(max);
                    closeWindow();
                }
            }.start();
        }
    }

    private void closeWindow() {
        mSMSView.setOnTouchListener(null);
        mWindowManager.removeView(mSMSView);
        stopSelf();
    }

    private void showWindow() {

        mShow = true;
        mSMSView = View.inflate(this, R.layout.window_sms_display, null);

        final Pair<Integer, Integer> screenSize = Utils.getScreenSize(this);
        final int windowHeight = screenSize.second - 100;
        final int windowWidth = screenSize.first - 2;

        WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                windowWidth, windowHeight, 10, 10,
                WindowManager.LayoutParams.TYPE_PHONE,
                //WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                PixelFormat.TRANSLUCENT);

        mParams.gravity = Gravity.CENTER;

        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(mSMSView, mParams);

        if(!mConfig.hasTimeout)
            getProgressBar().setVisibility(GONE);

        showSMS(mSMSContainer.next(), mSMSContainer.getTrackInfo());

        mSMSView.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeRight() {
                showSMS(mSMSContainer.previous(), mSMSContainer.getTrackInfo());
            }

            @Override
            public void onSwipeLeft() {
                showSMS(mSMSContainer.next(), mSMSContainer.getTrackInfo());
            }

            @Override
            public void onSwipeTop() {
                closeWindow();
            }

            @Override
            public void onSwipeBottom() {
                closeWindow();
            }
        });

        if (mConfig.hasTimeout)
            startCountDown();
    }

    private void showSMS(SMS sms, String trackInfo) {
        getTextView(R.id.txtTrackInfo).setText(trackInfo);
        getTextView(R.id.txtBody).setText(sms.body);
        getTextView(R.id.txtNumber).setText(sms.phoneNumber);
        getTextView(R.id.txtSender).setText(sms.sender);
    }

    private ProgressBar getProgressBar() {
        return (ProgressBar) mSMSView.findViewById(R.id.progTimer);
    }

    private TextView getTextView(@IdRes int id) {
        return (TextView) mSMSView.findViewById(id);
    }

    /**
     * just for testing purposes
     * @param intent
     */
    private void mockDecodeSMS(Intent intent) {
        ArrayList<String> rawMessages = intent.getStringArrayListExtra(SMSProxyReceiver.EXTRA_MOCK_MESSAGES);
        for (String rm : rawMessages) {
            String[] tokens = rm.split(";");
            mSMSContainer.add(new SMS(tokens[0], tokens[1], tokens[2]));
        }
    }

    private void decodeSMS(Intent intent) {
        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                Object[] pdusObj = (Object[]) bundle.get("pdus");
                String contactName = null,
                        phoneNumber = null,
                        body = null;
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    contactName = Utils.getContactName(this, phoneNumber);
                    body = currentMessage.getDisplayMessageBody();
                }
                mSMSContainer.add(new SMS(contactName, phoneNumber, body));
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception while decoding SMS" + e);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
