package home.crsk.smssplasher;

/**
 * Created by criskey on 1/1/2017.
 */

public class Config {

    public final boolean hasTimeout;

    public final int timeout;

    public Config(boolean hasTimeout, int timeout) {
        this.hasTimeout = hasTimeout;
        this.timeout = timeout;
    }
}
